# Pidipi - PI DIsplay PIctures

Create a digital frame with the [Pi Display](https://www.raspberrypi.org/documentation/hardware/display/)

## Hardware

### Raspberry Pi 7” Touchscreen Display

[Homepage](http://www.element14.com/PiDisplay)

Technical Specification:
  * 7” Touchscreen Display
  * Screen Dimensions: 194mm x 110mm x 20mm (including standoffs)
  * Viewable screen size: 155mm x 86mm
  * Screen Resolution 800 x 480 pixels
  * 10 finger capacitive touch
  * Connects to the Raspberry Pi board using a ribbon cable connected to the DSI port
  * Adapter board is used to power the display and convert the parallel signals from the display to the serial (DSI) port on the Raspberry Pi

## Install

### Prepare the SD Card

- Download the [latest Raspbian Lite image](https://downloads.raspberrypi.org/raspbian_lite_latest) from [Raspberry Pi website](https://www.raspberrypi.org/downloads/raspbian/)
- Write the image on a SD Card
- To [Enable the ssh](https://www.raspberrypi.org/documentation/remote-access/ssh/) add an empty file "ssh" on the boot partition of the SD card.
- To setup the Wifi edit, then copy the wpa_supplicant.conf file on the boot partition of the SD card.
- Put the SD Card in the Raspberry Pi

### Under Debian or Windows WSL to use Ansible

The first time install the requirements with :

```sh
sudo ./scripts/install-requirements.sh
```

Then each time you want to work with the playbook, activate a python virtual env with :

```sh
source ./scripts/init-venv
```

## Prepare a ssh key

```sh
ssh-keygen -t ed25519 -P "" -f $HOME/.ssh/ansible_pidipi_ed25519 -C "ansible@pidipi"
```

## Adding pictures with Dropbox and my Synology NAS

### Create a user "pidipi" withe the password "pidipi"

Create the folders:
- `input` for the new pictures
- `backup` to backup the full size pictures
- `gallery` to store the reduced pictures to fit the pi frame

### Setup a Cloud Sync task

With Dropbox and :
- Local path : /homes/pidipi/input
- Remote path : /ChoupouLand/CadrePhoto
- Sync direction : Bidirectional

## Setup the variable

### With Bitwarden 

```sh
export BW_LOGIN=youremail
./scripts/get-private-vars-from-bitwarden.sh pidipi
```

### Manual

Edit the file `group_vars/all.yml` and/or the file `hosts` as you wish.
You can also put your variables in a new file in `group_vars/private_external_vars.yml`

if not set the task will not change the timezone
`timezone: Europe/Paris`

if one of the wifi variables is not set the wifi will not be configured
`wifi_country: YourCountryCodeISO2`
`wifi_ssid: YourWifiName`
`wifi_passphrase: YourWifiPassword`

the path / user / password to mount your nas folder
`nas_path: //mynasdns/folder`
`nas_username: TheNasUsernameForPidipi`
`nas_password: TheNasPasswordForPidipi`

## Launch the playbook

```sh
cd ansible
ansible-playbook ansible/install-pidipi.yml
```

## Special thanks

- Framboise314 [For my previous setup](http://www.framboise314.fr/un-cadre-photo-avec-le-raspberry-pi/)
- Raspberrypi-spy [To discover FBI](https://www.raspberrypi-spy.co.uk/2017/02/how-to-display-images-on-raspbian-command-line-with-fbi/)
