#!/bin/bash

HOME_FOLDER=/home/pidipi
NAS_FOLDER=$HOME_FOLDER/nas
LOCAL_PICTURE_FOLDER=$HOME_FOLDER/pictures

# MOUNT
mount $NAS_FOLDER
if [ $? -gt 0 ]
then
        echo "Error mount NAS folder (exit $?)"
        umount $NAS_FOLDER
        exit 1
fi


# IMPORT
INPUT_FOLDER=$NAS_FOLDER/input/*
BACKUP_FOLDER=$NAS_FOLDER/backup
GALLERY_FOLDER=$NAS_FOLDER/gallery

for file_input in $INPUT_FOLDER
do
        [ -f "$file_input" ] || continue
        echo $file_input
        filename=${file_input##*/}
        # prefix=$(date +%Y-%m-%d_%H%M%S)
        file_backup=$BACKUP_FOLDER/$filename
        # copy file to backup
        cp --backup=numbered $file_input $file_backup
        # convert file to gallery
        file_gallery=$GALLERY_FOLDER/$filename
        convert $file_input -resize 800x480 $file_gallery
        # remove file
        rm $file_input
done

# sync gallery folder to local folder
rsync_result=$(rsync -av --delete --exclude '*.db' --stats $GALLERY_FOLDER/ $LOCAL_PICTURE_FOLDER)
has_not_changed=$(echo "${rsync_result}" | grep "Number of deleted files: 0.*Number of regular files transferred: 0")

# WE KILL THE SLIDESHOW if something changed
[ -z "${has_not_changed}" ] && killall fbi

# UMOUNT
sleep 2
umount $NAS_FOLDER
