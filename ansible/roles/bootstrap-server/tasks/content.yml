---
- name: Remove 'pi' user
  # the first time we don't delete the user because we run the playbook as this user.
  user:
    name: pi
    remove: yes
    state: absent
  when: ansible_user != "pi"

- name: Add deploy user
  user:
    name: "{{ deploy_username }}"
    password: "*"
    shell: /bin/bash

- name: Set authorized key taken from file for deploy user
  authorized_key:
    user: "{{ deploy_username }}"
    state: present
    key: "{{ lookup('file', item) }}"
  with_items: "{{ deploy_public_keys }}"

- name: Add deploy user to sudoers
  lineinfile:
    dest: /etc/sudoers
    regexp: "{{ deploy_username }} ALL"
    line: "{{ deploy_username }} ALL=(ALL) NOPASSWD: ALL"
    state: present

- name: Change deploy user in playbook
  set_fact:
    ansible_user: "{{ hostvars[new_hostname].ansible_user }}"
    ansible_ssh_private_key_file: "{{ hostvars[new_hostname].ansible_ssh_private_key_file }}"

- name: Disallow password authentication
  lineinfile: dest=/etc/ssh/sshd_config
              regexp="^PasswordAuthentication"
              line="PasswordAuthentication no"
              state=present
  notify: Restart ssh

- name: Disallow root SSH access
  lineinfile: dest=/etc/ssh/sshd_config
              regexp="^PermitRootLogin"
              line="PermitRootLogin no"
              state=present
  notify: Restart ssh

- name: Set new hostname - {{ new_hostname }}"
  hostname:
    name: "{{ new_hostname }}"
  notify:
    - Restart dhcpcd

# this workaround task is mandatory for hostname renaming
# see https://github.com/ansible/ansible-modules-core/issues/2004
- name: Set new hostname - workaround - add {{ new_hostname }} to /etc/hosts
  lineinfile:
    state: present
    dest: /etc/hosts
    regexp: "^127.0.1.1\t{{ new_hostname }}$"
    line: "127.0.1.1\t{{ new_hostname }}"
  notify:
    - Restart dhcpcd

- name: Add hostname-wifi to the dhcpcd config
  become: true
  template:
    src: templates/dhcpcd.conf.j2
    dest: /etc/dhcpcd.conf
  notify: Restart dhcpcd

- name: Disable root password
  user:
    name: root
    password: '*'

- name: Check if user 'pi' exist
  getent:
    database: passwd
    key: pi
    fail_key: false

- name: Disable 'pi' password 
  user:
    name: pi
    password: '*'
  when: getent_passwd['pi']