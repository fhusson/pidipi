#!/bin/bash

#----------------------------------------------------------
# Variables
#----------------------------------------------------------
PROJECT_NAME="${1}"
ANSIBLE_VARS_FILE=ansible/group_vars/private_external_vars.yml
BW_ITEM_NAME="${PROJECT_NAME}_ansible_vars"
VERBOSE=1

#----------------------------------------------------------
# Functions
#----------------------------------------------------------
die() {
    printf "\033[31mERROR - ${1}\033[m\n"
    exit 1
}

usage() {
    echo "Generate the file '${ANSIBLE_VARS_FILE}' from a Bitwarden entry"
    echo "You must set BW_LOGIN=youraccountemail"
    echo "If not present, it will try to sudo install NPM and @bitwarden/cli"
    echo "The Bitwarden entry name must be 'projectname'_ansible_vars where projectname is the argument"
    echo ""
    echo "Usage: ${0} 'projectname'"
    exit 1
}

#----------------------------------------------------------
# Checks
#----------------------------------------------------------
[ -z "${PROJECT_NAME}" ] && usage

[ -z "${BW_LOGIN}" ] && die "BW_LOGIN is not set"

echo "Checking NPM install"
[ -z "$(npm --version)" ] && sudo apt install npm

echo "Checking Bitwarden CLI install"
[ -z "$(bw -v)" ] && sudo npm install -g @bitwarden/cli

echo "Bitwarden login for ${BW_LOGIN}"
bw logout > /dev/null 2>&1
[ -z "${BW_PASSWORD}" ] && echo "Please enter your Bitwarden"
export BW_SESSION=$(bw login "${BW_LOGIN}" "${BW_PASSWORD}" --raw)
[ -z "${BW_SESSION}" ] && die "BW_SESSION is not set"

# We get the vars from each fields of the entry
echo "Extracting variables from the Bitwarden entry ${BW_ITEM_NAME} to ${ANSIBLE_VARS_FILE}"
bw list items --search "${BW_ITEM_NAME}" | jq -r '.[0].fields | map([.name, .value] | join(": ")) | join("\n")' > "${ANSIBLE_VARS_FILE}"